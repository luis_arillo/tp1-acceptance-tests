package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CreationTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        throw new RuntimeException("not implemented yet!");
    }

    @Test
    public void startWithNoWorkBooks() {
        assertTrue(testDriver.workBooksNames().isEmpty());
    }

    @Test
    public void createOneWorkBook() {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertThat(testDriver.workBooksNames(), contains("tecnicas"));
    }

    @Test
    public void createMultipleWorkBooks() {
        testDriver.createNewWorkBookNamed("tecnicas 1");
        testDriver.createNewWorkBookNamed("tecnicas 2");

        assertThat(testDriver.workBooksNames(), contains("tecnicas 1", "tecnicas 2"));
        assertThat(testDriver.workBooksNames(), not(contains("tecnicas")));
    }

    @Test
    public void workBookStartsWithDefaultWorkSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("default"));
    }

    @Test
    public void createOneWorkSheet() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "other");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("other"));
    }

    @Test
    public void createAdditionalWorkSheets() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "firstAdditionalWorksheet");
        testDriver.createNewWorkSheetNamed("tecnicas", "secondAdditionalWorksheet");

        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("firstAdditionalWorksheet"));
        assertThat(testDriver.workSheetNamesFor("tecnicas"), contains("secondAdditionalWorksheet"));
    }

}
